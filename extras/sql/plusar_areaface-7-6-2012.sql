-- phpMyAdmin SQL Dump
-- version 3.4.10.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 07-06-2012 a las 08:24:59
-- Versión del servidor: 5.0.95
-- Versión de PHP: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `plusar_areaface`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brw_files`
--

CREATE TABLE IF NOT EXISTS `brw_files` (
  `id` bigint(20) unsigned NOT NULL auto_increment,
  `name` varchar(255) collate utf8_unicode_ci NOT NULL,
  `record_id` int(10) unsigned NOT NULL,
  `model` varchar(255) collate utf8_unicode_ci NOT NULL,
  `description` varchar(255) collate utf8_unicode_ci NOT NULL,
  `category_code` char(10) collate utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `category_code` (`category_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brw_images`
--

CREATE TABLE IF NOT EXISTS `brw_images` (
  `id` bigint(20) unsigned NOT NULL auto_increment,
  `name` varchar(255) collate utf8_unicode_ci NOT NULL,
  `record_id` int(10) unsigned NOT NULL,
  `model` varchar(255) collate utf8_unicode_ci NOT NULL,
  `description` varchar(255) collate utf8_unicode_ci NOT NULL,
  `category_code` char(10) collate utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `category_code` (`category_code`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=65 ;

--
-- Volcado de datos para la tabla `brw_images`
--

INSERT INTO `brw_images` (`id`, `name`, `record_id`, `model`, `description`, `category_code`, `created`, `modified`) VALUES
(14, '100-de-MILAN.jpg', 80, 'Post', '', 'Foto', '2011-10-15 21:40:24', '2011-10-15 21:40:24'),
(15, '100-de-MILAN.jpg', 83, 'Post', '', 'Foto', '2011-10-15 21:47:47', '2011-10-15 21:47:47'),
(16, 'DJ-ALE-DJ-MAURI-DJ-PETY-2.jpg', 120, 'Post', '', 'Foto', '2011-10-26 03:25:41', '2011-10-26 03:25:41'),
(17, 'TRAFICS-1.jpg', 124, 'Post', '', 'Foto', '2011-10-26 03:36:17', '2011-10-26 03:36:17'),
(18, 'DSCN1073.JPG', 125, 'Post', '', 'Foto', '2011-10-26 15:50:04', '2011-10-26 15:50:04'),
(19, 'TRAFICS-1.jpg', 128, 'Post', '', 'Foto', '2011-10-26 18:42:03', '2011-10-26 18:42:03'),
(20, 'DJ-ALE-DJ-MAURI-DJ-PETY-3.jpg', 129, 'Post', '', 'Foto', '2011-10-26 18:54:31', '2011-10-26 18:54:31'),
(21, 'DSCN2391.JPG', 133, 'Post', '', 'Foto', '2011-10-26 19:06:51', '2011-10-26 19:06:51'),
(22, 'AREA-institucional-2.jpg', 135, 'Post', '', 'Foto', '2011-10-26 19:16:53', '2011-10-26 19:16:53'),
(23, 'AREA-institucional-2.jpg', 136, 'Post', '', 'Foto', '2011-10-29 15:58:37', '2011-10-29 15:58:37'),
(24, 'DSCN2391.JPG', 137, 'Post', '', 'Foto', '2011-10-29 16:00:35', '2011-10-29 16:00:35'),
(25, 'DJ-ALE-DJ-MAURI-DJ-PETY-1.jpg', 138, 'Post', '', 'Foto', '2011-10-29 16:13:14', '2011-10-29 16:13:14'),
(26, 'DJ-ALE-halloween-DJ-PETY.jpg', 142, 'Post', '', 'Foto', '2011-10-29 16:31:00', '2011-10-29 16:31:00'),
(27, 'HALLOWEEN.jpg', 143, 'Post', '', 'Foto', '2011-10-29 16:37:19', '2011-10-29 16:37:19'),
(28, 'DJ-ALE-halloween-DJ-RAKA-DJ-MARIAN.jpg', 144, 'Post', '', 'Foto', '2011-10-29 16:41:18', '2011-10-29 16:41:18'),
(29, 'HALLOWEEN-3.jpg', 148, 'Post', '', 'Foto', '2011-10-29 16:55:43', '2011-10-29 16:55:43'),
(30, 'DSCN2375.JPG', 149, 'Post', '', 'Foto', '2011-10-29 17:01:24', '2011-10-29 17:01:24'),
(31, 'DSCN9266.JPG', 151, 'Post', '', 'Foto', '2011-10-29 17:24:48', '2011-10-29 17:24:48'),
(32, 'DSCN8807.JPG', 152, 'Post', '', 'Foto', '2011-10-29 17:28:10', '2011-10-29 17:28:10'),
(33, 'AREA-institucional-2011.JPG', 153, 'Post', '', 'Foto', '2011-10-29 17:32:34', '2011-10-29 17:32:34'),
(34, 'AREA-carlos-paz-punto-alvear.jpg', 234, 'Post', '', 'Foto', '2011-11-26 14:57:18', '2011-11-26 14:57:18'),
(35, 'AREA-autos-publicas.jpg', 235, 'Post', '', 'Foto', '2011-11-26 15:14:26', '2011-11-26 15:14:26'),
(36, 'AREA-carlos-paz-molino-rojo.jpg', 236, 'Post', '', 'Foto', '2011-11-26 15:15:16', '2011-11-26 15:15:16'),
(37, 'AREA-despedida-de-temporada.jpg', 237, 'Post', '', 'Foto', '2011-11-26 15:17:18', '2011-11-26 15:17:18'),
(38, 'ELIAS-RAMPELLO-CEREBRO-MAGICO.jpg', 238, 'Post', '', 'Foto', '2011-11-26 15:18:32', '2011-11-26 15:18:32'),
(39, 'REMERA-AREA-ME-GUSTA.jpg', 239, 'Post', '', 'Foto', '2011-11-26 15:19:35', '2011-11-26 15:19:35'),
(40, 'AREA-gafas.jpg', 241, 'Post', '', 'Foto', '2011-11-26 15:23:31', '2011-11-26 15:23:31'),
(41, 'BIZARRA-AREA-VIP-INSTITUCIONAL.JPG', 242, 'Post', '', 'Foto', '2011-11-26 15:25:24', '2011-11-26 15:25:24'),
(42, 'ELIAS-RAMPELLO-CEREBRO-MAGICO.jpg', 243, 'Post', '', 'Foto', '2011-11-26 15:26:45', '2011-11-26 15:26:45'),
(43, 'AREA-gafas.jpg', 246, 'Post', '', 'Foto', '2011-11-26 15:35:01', '2011-11-26 15:35:01'),
(44, 'NAVIDAD-2011.jpg', 247, 'Post', '', 'Foto', '2011-12-19 13:41:35', '2011-12-19 13:41:35'),
(45, 'MG-3567.JPG', 257, 'Post', '', 'Foto', '2012-01-13 22:32:09', '2012-01-13 22:32:09'),
(46, 'MG-3494.JPG', 258, 'Post', '', 'Foto', '2012-01-13 22:35:39', '2012-01-13 22:35:39'),
(47, 'MG-3508.JPG', 259, 'Post', '', 'Foto', '2012-01-13 22:42:38', '2012-01-13 22:42:38'),
(48, 'MG-3521.JPG', 260, 'Post', '', 'Foto', '2012-01-13 23:01:39', '2012-01-13 23:01:39'),
(49, 'ESPUMA-INTITUCIONAL.JPG', 267, 'Post', '', 'Foto', '2012-01-13 23:32:15', '2012-01-13 23:32:15'),
(50, 'ESPUMA-2012.jpg', 268, 'Post', '', 'Foto', '2012-01-13 23:33:55', '2012-01-13 23:33:55'),
(51, 'ELIAZIM-INSTITUCIUNAL.JPG', 269, 'Post', '', 'Foto', '2012-01-13 23:39:57', '2012-01-13 23:39:57'),
(52, 'ESPUMA-2012.jpg', 273, 'Post', '', 'Foto', '2012-01-14 00:32:24', '2012-01-14 00:32:24'),
(53, 'RETRO-o-BIZARRA-6.JPG', 306, 'Post', '', 'Foto', '2012-02-24 14:33:24', '2012-02-24 14:33:24'),
(54, 'RETRO-o-BIZARRA-6.JPG', 307, 'Post', '', 'Foto', '2012-02-24 17:40:19', '2012-02-24 17:40:19'),
(55, 'AREA-11-ANIVERSARIO.jpg', 359, 'Post', '', 'Foto', '2012-04-09 11:08:37', '2012-04-09 11:08:37'),
(56, 'SABINA-SIN-ANESTESIA-FOTO-0.jpg', 417, 'Post', '', 'Foto', '2012-05-16 11:20:47', '2012-05-16 11:20:47'),
(57, 'SABINA-SIN-ANESTESIA-FOTO-1.jpg', 418, 'Post', '', 'Foto', '2012-05-16 11:25:59', '2012-05-16 11:25:59'),
(62, 'LOS-CUMPLE-DE-MAYO.jpg', 16, 'Post', '', 'Foto', '2012-05-23 13:39:43', '2012-05-23 13:39:43'),
(63, 'LOS-CUMPLE-DE-MAYO.jpg', 28, 'Post', '', 'Foto', '2012-05-23 19:38:56', '2012-05-23 19:38:56'),
(61, 'RETRO-o-BIZARRA-8.jpg', 14, 'Post', '', 'Foto', '2012-05-23 13:16:37', '2012-05-23 13:16:37'),
(64, 'RETRO-o-BIZARRA-8.jpg', 30, 'Post', '', 'Foto', '2012-05-23 19:46:01', '2012-05-23 19:46:01');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brw_users`
--

CREATE TABLE IF NOT EXISTS `brw_users` (
  `id` int(5) unsigned NOT NULL auto_increment,
  `email` varchar(50) collate utf8_unicode_ci NOT NULL,
  `password` varchar(255) collate utf8_unicode_ci NOT NULL,
  `last_login` datetime NOT NULL,
  `root` tinyint(1) NOT NULL default '1',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `username` (`email`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `brw_users`
--

INSERT INTO `brw_users` (`id`, `email`, `password`, `last_login`, `root`, `created`, `modified`) VALUES
(5, 'test@test.com', '57a63f781e35b699a56fc1a34cdbfeedb620447f', '0000-00-00 00:00:00', 1, '2012-05-21 16:05:58', '2012-05-21 16:05:58'),
(4, 'areadata@hotmail.com', '613ba16f7bf14c9ed3d5e7ace62a0ffa8386f158', '0000-00-00 00:00:00', 1, '2011-10-08 00:23:30', '2011-10-08 00:23:30');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `periodos`
--

CREATE TABLE IF NOT EXISTS `periodos` (
  `id` int(10) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `periodos`
--

INSERT INTO `periodos` (`id`, `name`, `created`, `modified`) VALUES
(1, 'Minuto/s', '2012-05-22 12:57:12', '2012-05-22 12:57:12'),
(2, 'Hora/s', '2012-05-22 12:57:25', '2012-05-22 12:57:25'),
(3, 'Día/s', '2012-05-22 12:57:52', '2012-05-22 12:57:52'),
(4, 'Semana/s', '2012-05-22 12:58:09', '2012-05-22 12:58:09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(10) NOT NULL auto_increment,
  `mensaje` text collate utf8_unicode_ci NOT NULL,
  `link` varchar(300) collate utf8_unicode_ci default NULL,
  `fecha_hora_publicacion` datetime NOT NULL,
  `una_vez` tinyint(1) NOT NULL default '0',
  `repetir_cada` int(4) default NULL,
  `cant_repeticiones` int(3) NOT NULL,
  `cant_publicadas` int(3) NOT NULL default '0',
  `repetir_cada_show` int(11) default NULL,
  `periodo_id` int(11) default NULL,
  `fecha_hora_ultima_publicacion` datetime default NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `fecha_hora_publicacion` (`fecha_hora_publicacion`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=80 ;

--
-- Volcado de datos para la tabla `posts`
--

INSERT INTO `posts` (`id`, `mensaje`, `link`, `fecha_hora_publicacion`, `una_vez`, `repetir_cada`, `cant_repeticiones`, `cant_publicadas`, `repetir_cada_show`, `periodo_id`, `fecha_hora_ultima_publicacion`, `created`, `modified`) VALUES
(15, 'AMIGOS,.. ESTA ES LA BANDA Q MAÑANA JUEVES 24, VA A INAUGURAR LA PRIMER "FIESTA RETRO vs. BIZARRA" de AREAVIP.!!\r\n\r\n:::FEEDBACK::: JANET (en voz) - MARCELO (guitarra) Y JUAN DIEGO (teclados) de SAN LUIS CAPITAL\r\n\r\nELLOS HACEN UN TRIBUTO A LOS 80 y 90 DE BANDAS INTERNACIONALES COMO:: ROXETTE - QUEEN - KISS - BEATLES - SHANIA TWAIN - ETC..\r\nY NACIONALES:::: SODA STEREO - VIRUS - FITO PAEZ - CHARLY GARCÍA ETC...\r\n\r\nCON UNA PRODUCCIÓN IMPECABLE EN CADA UNA DE LAS CANCIONES , HACEN UN SHOW CON MAS DE 30 TEMAS QUE NOS LLENAN DE RECUERDOS DE AQUELLAS ÉPOCAS....\r\n\r\nREALMENTE RECOMENDABLES PARA COMENZAR ESTE PROXIMO JUEVES 24 LA "FIESTA RETRO vs. BIZARRA" DE AREAVIP.\r\n\r\n\r\n\r\n\r\nhttp://www.youtube.com/watch?v=X4YWhRZ55Qk&feature=relmfu', 'http://www.youtube.com/watch?v=X4YWhRZ55Qk&feature=relmfu', '2012-05-23 13:40:00', 1, 60, 3, 0, 1, 2, NULL, '2012-05-23 13:36:12', '2012-05-23 13:36:12'),
(16, 'ATENCION CUMPLEAÑEROS de MAYO.!! ESTE SAB 26/5 EN AREAVIP y AREADANCE\r\n\r\n::FESTEJAMOS LOS CUMPLES DE MAYO::\r\n\r\nATENCION:: SI CUMPLISTE LOS AÑOS DURANTE ESTE MES PREPARATE PARA SER NUESTRO ANFITRION DE LA NOCHE!!\r\n\r\nESTE SAB 26/5 FESTELAJOS TU CUMPLE Y EL DE TODOS LOS Q CUMPLIERON AÑOS EN MAYO!..\r\n\r\nENVIANOS A NUESTRO FACEBOOK TU LISTA DE 4 UNVITADOS ESPECIALES.. Y EL SABADO TE RECIBIMOS A VOS JUNTO A TUS INVITADOS CON ENTRADA SIN CARGO + LA TROTA + EL BRINDIS + EL SHOW Y CANTOBAR EN VIVO!!\r\n\r\nESTA PROMO ES VALIDA DESDE LAS 00:00 h. HASTA ANTES DE LAS 03:00 hs.\r\n\r\nENTRA YA EN NUESTRO FACE Y DEJANOS TU LISTA DE INVITADOS ESPECIALES!..\r\n\r\n\r\nESCUCHA ACA LA PUBLI QUE SUENA EN TODAS LAS RADIOS.!!\r\n\r\nhttp://soundcloud.com/area-disco-san-jose/este-jueves-24-vispera-del\r\n\r\nAQUI TODAS LAS FOTOS DE LOS SABADOS ANTERIORES, BUSCATE, ENCONTRATE Y ETIKETATE!!..\r\n\r\nhttp://www.facebook.com/profile.php?id=1684553995&sk=photos\r\n\r\nFACEBOOCK\r\n\r\nhttp://www.facebook.com/profile.php?id=1684553995\r\n\r\nMas info en WWW.AREADISCO.COM.AR', '', '2012-05-23 13:50:00', 1, 60, 3, 0, 1, 2, NULL, '2012-05-23 13:39:43', '2012-05-23 15:53:04'),
(17, 'URGENTEEEE!!! ESCUCHA ESTA PUBLI y ADELANTATE AL FIESTON Q SE VIENE MAÑANA JUEVES en AREAVIP!!..\r\n\r\nESTE JUEVES 24 (vispera del feriado nacional) AREAVIP presenta la primer..\r\n\r\n"FIESTA RETRO vs. BIZARRA"\r\n\r\nESTE JUEVES 24/5 REGRESA AL VIP DE AREA LAS FIESTA MAS LOCA Y DECONTROLADA Q TODOS ESTABAN ESPERANDO..\r\n\r\n>>FIESTA RETRO vs. BIZARRA<<\r\n\r\nLAS DOS MEJORES FIESTAS DE AREAVIP EN UNA SOLA NOCHE!!\r\n\r\nSI SOS FAN O TE GUSTAN LAS CANCIONES RETRO o BIZARRAS NO PODES PERDERTE ESTA SUPER NOCHEEE\r\n\r\nPROXIMO JUEVES DESDE LA HORA 00:00 EN AREAVIP!!\r\n\r\nRECORDA!,. JUEVES 24/5 >>FIESTA RETRO vs. BIZARRA<<\r\n\r\nESCUCHA ACA LA PUBLI QUE SUENA EN TODAS LAS RADIOS.!!\r\n\r\nhttp://soundcloud.com/area-disco-san-jose/este-jueves-24-vispera-del\r\n\r\nAQUI TODAS LAS FOTOS DE LOS SABADOS ANTERIORES, BUSCATE, ENCONTRATE Y ETIKETATE!!..\r\n\r\nhttp://www.facebook.com/profile.php?id=1684553995&sk=photos\r\n\r\nFACEBOOCK\r\n\r\nhttp://www.facebook.com/profile.php?id=1684553995\r\n\r\nMas info en WWW.AREADISCO.COM.AR', 'http://soundcloud.com/area-disco-san-jose/este-jueves-24-vispera-del', '2012-05-23 14:00:00', 1, 60, 3, 0, 1, 2, NULL, '2012-05-23 13:42:53', '2012-05-23 13:42:53'),
(18, 'MAÑANAAAA!!!.. JUEVES 24 (vispera del feriado) REGRESA EL EVENTO MAS ESPERADO POR TODOS LOS CONCURRENTES DEL "AREA VIP"\r\n\r\nESTE JUEVES LLEGA.. LA PRIMER "FIESTA RETRO vs. BIZARRA"\r\n\r\nAPERTURA 00:00 hs.\r\n\r\nCOMIENZO DEL SHOW 01:30 a cargo de la agrupacion "FEEDBACK" trayendo a nuestras memorias LO MEJOR de la musica NACIONAL e INTERNACIONAL de LOS 80s y 90s\r\n\r\nYA ESTAN TODOS INVITADOS A LA FIESTA MAS DIVERTIDA DE AREA VIP!.\r\n\r\n\r\n\r\n', 'http://www.facebook.com/photo.php?fbid=388437487875797&set=a.312183565501190.97572.100001286464960&type=1', '2012-05-23 14:10:00', 1, 60, 3, 0, 1, 2, NULL, '2012-05-23 13:45:00', '2012-05-23 13:45:00'),
(20, 'QUE BUENO ES SABER Q MAÑANA VOLVEMOS A ESTAR DE FIESTA!!..\r\n\r\nCUANTO ESPERAMOS VOLVER A VIVIR ESAS MAGICAS NOCHES DE MUSICA "RETRO y BIZARRAS"\r\n\r\nSOLO UN DIA PARA VOLVER A VIAJAR ATRAVES DEL TIEMO!!!', 'http://soundcloud.com/area-disco-san-jose/este-jueves-24-vispera-del', '2012-05-23 14:30:00', 0, 60, 3, 1, 1, 2, NULL, '2012-05-23 13:48:57', '2012-05-23 16:30:05'),
(14, 'SOLO "1 DIA" PARA VIVIR LA TAN ESPERADA >>FIESTA RETRO vs. BIZARRA<<\r\n\r\nMAÑANA JUEVES 24 (vispera del feriado nacional) AREAVIP presenta la primer..\r\n\r\n"FIESTA RETRO vs. BIZARRA"\r\n\r\nESTE JUEVES 24/5 REGRESA AL VIP DE AREA LAS FIESTA MAS LOCA Y DECONTROLADA Q TODOS ESTABAN ESPERANDO..\r\n\r\n>>FIESTA RETRO vs. BIZARRA<<\r\n\r\nLAS DOS MEJORES FIESTAS DE AREAVIP EN UNA SOLA NOCHE!!\r\n\r\nSI SOS FAN O TE GUSTAN LAS CANCIONES RETRO o BIZARRAS NO PODES PERDERTE ESTA SUPER NOCHEEE\r\n\r\nPROXIMO JUEVES DESDE LA HORA 00:00 EN AREAVIP!!\r\n\r\nRECORDA!,. JUEVES 24/5 >>FIESTA RETRO vs. BIZARRA<<\r\n\r\nESCUCHA ACA LA PUBLI QUE SUENA EN TODAS LAS RADIOS.!!\r\n\r\nhttp://soundcloud.com/area-disco-san-jose/este-jueves-24-vispera-del\r\n\r\nAQUI TODAS LAS FOTOS DE LOS SABADOS ANTERIORES, BUSCATE, ENCONTRATE Y ETIKETATE!!..\r\n\r\nhttp://www.facebook.com/profile.php?id=1684553995&sk=photos\r\n\r\nFACEBOOCK\r\n\r\nhttp://www.facebook.com/profile.php?id=1684553995\r\n\r\nMas info en WWW.AREADISCO.COM.AR', '', '2012-05-23 13:20:00', 1, 60, 3, 3, 1, 2, NULL, '2012-05-23 13:16:37', '2012-05-23 22:42:06'),
(26, 'MAÑANA!! JUEVES en AREAVIP!! llega la "FIESTA RETRO vs. BIZARRA"\r\n\r\nESTE JUEVES 24 (vispera del feriado nacional) AREAVIP presenta la primer..\r\n\r\n"FIESTA RETRO vs. BIZARRA"\r\n\r\nESTE JUEVES 24/5 REGRESA AL VIP DE AREA LAS FIESTA MAS LOCA Y DECONTROLADA Q TODOS ESTABAN ESPERANDO..\r\n\r\n>>FIESTA RETRO vs. BIZARRA<<\r\n\r\nLAS DOS MEJORES FIESTAS DE AREAVIP EN UNA SOLA NOCHE!!\r\n\r\nSI SOS FAN O TE GUSTAN LAS CANCIONES RETRO o BIZARRAS NO PODES PERDERTE ESTA SUPER NOCHEEE\r\n\r\nPROXIMO JUEVES DESDE LA HORA 00:00 EN AREAVIP!!\r\n\r\nRECORDA!,. JUEVES 24/5 >>FIESTA RETRO vs. BIZARRA<<\r\n\r\nESCUCHA ACA LA PUBLI QUE SUENA EN TODAS LAS RADIOS.!!\r\n\r\nhttp://soundcloud.com/area-disco-san-jose/este-jueves-24-vispera-del\r\n\r\nAQUI TODAS LAS FOTOS DE LOS SABADOS ANTERIORES, BUSCATE, ENCONTRATE Y ETIKETATE!!..\r\n\r\nhttp://www.facebook.com/profile.php?id=1684553995&sk=photos\r\n\r\nFACEBOOCK\r\n\r\nhttp://www.facebook.com/profile.php?id=1684553995\r\n\r\nMas info en WWW.AREADISCO.COM.AR', 'http://soundcloud.com/area-disco-san-jose/este-jueves-24-vispera-del', '2012-05-23 19:40:00', 0, 60, 5, 3, 1, 2, NULL, '2012-05-23 19:35:35', '2012-05-23 19:52:43'),
(27, 'AMIGOS,.. // PRESTEN ATENCION // ESTA ES LA BANDA Q VA A INAUGURAR MAÑANA JUEVES 24 LA PRIMER "FIESTA RETRO vs. BIZARRA"\r\n\r\n:::FEEDBACK::: JANET (en voz) - MARCELO (guitarra) Y JUAN DIEGO (teclados) de SAN LUIS CAPITAL\r\n\r\nELLOS HACEN UN TRIBUTO A LOS 80 y 90 DE BANDAS INTERNACIONALES COMO:: ROXETTE - QUEEN - KISS - BEATLES - SHANIA TWAIN - ETC..\r\nY NACIONALES:::: SODA STEREO - VIRUS - FITO PAEZ - CHARLY GARCÍA ETC...\r\n\r\nCON UNA PRODUCCIÓN IMPECABLE EN CADA UNA DE LAS CANCIONES , HACEN UN SHOW CON MAS DE 30 TEMAS QUE NOS LLENAN DE RECUERDOS DE AQUELLAS ÉPOCAS....\r\n\r\nREALMENTE RECOMENDABLES PARA COMENZAR ESTE PROXIMO JUEVES 24 LA "FIESTA RETRO vs. BIZARRA" DE AREAVIP..\r\n\r\n\r\n\r\n\r\nhttp://www.youtube.com/watch?v=X4YWhRZ55Qk&feature=relmfu', 'http://www.youtube.com/watch?v=X4YWhRZ55Qk&feature=relmfu', '2012-05-23 19:50:00', 0, 60, 5, 0, 1, 2, NULL, '2012-05-23 19:36:43', '2012-05-23 19:36:43'),
(28, 'ESTE SAB 26/5 EN AREAVIP y AREADANCE\r\n\r\n::FESTEJAMOS LOS CUMPLES DE MAYO::\r\n\r\nATENCION:: SI CUMPLISTE LOS AÑOS DURANTE ESTE MES PREPARATE PARA SER NUESTRO ANFITRION DE LA NOCHE!!\r\n\r\nESTE SAB 26/5 FESTELAJOS TU CUMPLE Y EL DE TODOS LOS Q CUMPLIERON AÑOS EN MAYO!..\r\n\r\nENVIANOS A NUESTRO FACEBOOK TU LISTA DE 4 UNVITADOS ESPECIALES.. Y EL SABADO TE RECIBIMOS A VOS JUNTO A TUS INVITADOS CON ENTRADA SIN CARGO + LA TROTA + EL BRINDIS + EL SHOW Y CANTOBAR EN VIVO!!\r\n\r\nESTA PROMO ES VALIDA DESDE LAS 00:00 h. HASTA ANTES DE LAS 03:00 hs.\r\n\r\nENTRA YA EN NUESTRO FACE Y DEJANOS TU LISTA DE INVITADOS ESPECIALES!..\r\n\r\n\r\nESCUCHA ACA LA PUBLI QUE SUENA EN TODAS LAS RADIOS.!!\r\n\r\nhttp://soundcloud.com/area-disco-san-jose/este-jueves-24-vispera-del\r\n\r\nAQUI TODAS LAS FOTOS DE LOS SABADOS ANTERIORES, BUSCATE, ENCONTRATE Y ETIKETATE!!..\r\n\r\nhttp://www.facebook.com/profile.php?id=1684553995&sk=photos\r\n\r\nFACEBOOCK\r\n\r\nhttp://www.facebook.com/profile.php?id=1684553995\r\n\r\nMas info en WWW.AREADISCO.COM.AR', '', '2012-05-23 20:00:00', 0, 60, 5, 5, 1, 2, NULL, '2012-05-23 19:38:56', '2012-05-23 21:00:06'),
(29, 'jajaa CADA VEZ FALTA MENOS PARA VOLVER A VIVIR UNA DE LAS MEJORES NOCHES DE AREAVIP!!..\r\n\r\nMAÑANA JUEVES 24 regresa la tan esperada >>FIESTA RETRO vs. BIZARRA<<\r\n\r\nACA TE DEJAMOS UN BUEN BIZARRO Q VAMOS A BAILAR A PLENOOOO!!!..', 'http://www.youtube.com/watch?v=wtUD06B0gXs', '2012-05-23 20:10:00', 0, 60, 5, 1, 1, 2, NULL, '2012-05-23 19:43:28', '2012-05-23 20:13:03'),
(30, 'SOLO "1 DIA" PARA VIVIR LA TAN ESPERADA >>FIESTA RETRO vs. BIZARRA<<\r\n\r\nESTE JUEVES 24 (vispera del feriado nacional) AREAVIP presenta la primer..\r\n\r\n"FIESTA RETRO vs. BIZARRA"\r\n\r\nESTE JUEVES 24/5 REGRESA AL VIP DE AREA LAS FIESTA MAS LOCA Y DECONTROLADA Q TODOS ESTABAN ESPERANDO..\r\n\r\n>>FIESTA RETRO vs. BIZARRA<<\r\n\r\nLAS DOS MEJORES FIESTAS DE AREAVIP EN UNA SOLA NOCHE!!\r\n\r\nSI SOS FAN O TE GUSTAN LAS CANCIONES RETRO o BIZARRAS NO PODES PERDERTE ESTA SUPER NOCHEEE\r\n\r\nPROXIMO JUEVES DESDE LA HORA 00:00 EN AREAVIP!!\r\n\r\nRECORDA!,. JUEVES 24/5 >>FIESTA RETRO vs. BIZARRA<<\r\n\r\nESCUCHA ACA LA PUBLI QUE SUENA EN TODAS LAS RADIOS.!!\r\n\r\nhttp://soundcloud.com/area-disco-san-jose/este-jueves-24-vispera-del\r\n\r\nAQUI TODAS LAS FOTOS DE LOS SABADOS ANTERIORES, BUSCATE, ENCONTRATE Y ETIKETATE!!..\r\n\r\nhttp://www.facebook.com/profile.php?id=1684553995&sk=photos\r\n\r\nFACEBOOCK\r\n\r\nhttp://www.facebook.com/profile.php?id=1684553995\r\n\r\nMas info en WWW.AREADISCO.COM.AR', '', '2012-05-23 20:20:00', 0, 60, 5, 5, 1, 2, NULL, '2012-05-23 19:46:01', '2012-05-23 21:20:07'),
(32, 'siiiiiiiii!!! Cuantas GANAS de BAILAR y VOLVER el TIEMPO ATRAS!!..\r\n\r\nMAÑANA VOLVEMO a estar de FIESTA en la primer >>FIESTA RETRO vs. BIZARRA<<\r\n\r\nYa quiero que sea jueves!! QUIERO JODAAA.!!! QUIERO AMIGOOSS!! QUIERO AREAAA!!', 'http://www.youtube.com/watch?v=2I93gFx3gmc&feature=related', '2012-05-23 20:30:00', 0, 60, 5, 0, 1, 2, NULL, '2012-05-23 19:50:54', '2012-05-23 19:50:54'),
(55, 'wauuuu!! es increhible haber llegado a este tan esperado FINDE!!..\r\n\r\nYa es VIERNEEEESSSSS!!! mañana VOLVEMOS al PARAISO DE LAS MUJERES!!\r\n\r\nMAÑANA volvemos a estar de FIESTA en AREAAAAAAAAA!!!', 'http://www.youtube.com/watch?v=LRYNWfJzfVE', '2012-06-01 12:00:00', 0, 10080, 3, 3, 1, 4, NULL, '2012-06-01 11:46:10', '2012-06-01 12:02:03'),
(34, 'Wauuu!!! ya casi casi estamos adentro de areaaaa!!!\r\n\r\nEn un rato volvemos a vernos amigooss!!\r\n\r\nQuiero FIESTA.!!! los QUIERO a USTEDESSSSSSS!!..', 'http://www.youtube.com/watch?v=wmmqmOlVm98', '2012-05-26 23:59:00', 1, NULL, 1, 1, NULL, NULL, NULL, '2012-05-26 22:42:57', '2012-05-26 23:59:02'),
(35, 'AMIGOOOSSS!! esta noche FESTEJAMOS todos los CUMPLES de MAYOOO!!..\r\n\r\nRecuerden q todos los BENEFICIOS son validos Hasta antes de las 3\r\n\r\nNos vemos en un rato nada mas fiesterooosssss!!...', '', '2012-05-26 23:00:00', 1, NULL, 1, 1, NULL, NULL, NULL, '2012-05-26 22:47:11', '2012-05-26 23:00:03'),
(36, 'Si supieras las ganas de fiesta Q hace en este momento...\r\n\r\nNi te explico!!..\r\n\r\nYa quiero estar de caravanaaaa!! ya quiero AREAAAAAAAA!!..', '', '2012-05-26 23:30:00', 1, NULL, 1, 1, NULL, NULL, NULL, '2012-05-26 22:56:50', '2012-05-26 23:30:04'),
(37, 'Naaaa!! ya no veo la hora de estar en AREAA!!..\r\n\r\nEn un RATO comenzamos a FESTEJAR todos LOS CUMPLES de MAYO!..\r\n\r\nRecuerden q hoy la JODA empieza mas TEMPRANO..!!', '', '2012-05-27 00:30:00', 1, NULL, 1, 1, NULL, NULL, NULL, '2012-05-26 22:59:22', '2012-05-27 00:30:03'),
(38, 'SIIIIIII.!!! YA ARRANCAMOS LA NOCHE de AREAVIP con los mejores VÍDEOS en PANTALLA HD.\r\n\r\nEn una hora aproxi COMIENZA el SHOW en vivo de LOS COVERNICOLAS haciendo la mejor música NACIONAL e INTERNACIONAL + el CANTOBAR en vivo para todos LOS CUMPLEAÑEROS de MAYOOO!!..\r\n\r\nA prepararse areaaaa!!.. nos vemos en minutos!!', '', '2012-05-27 01:00:00', 1, NULL, 1, 1, NULL, NULL, NULL, '2012-05-26 23:03:45', '2012-05-27 01:00:03'),
(39, 'YA SON LA 1:30 hs ES LA HORA JUSTA PARA LEVANTAR LOS ANIMOS Y EMPEZAR A VIVIR LA NOCHE..\r\n\r\nYA COMENZAMOS A RECIBIR a TODOS LOS CUMPLEAÑEROS de MAYO en el VIP de AREA..\r\n\r\nEN 3O MINUTOS MAS ABRIMOS EL INGESO A TODOS LOS CUMPLEAÑEROS del AREADANCE juntos con sus INVITADOS ESPECIALES.!!!\r\n\r\nNO SE OLVIDEN Q LA FIESTA DE ESTA NOCHE ARRANCA MAS TEMPRANO KE NUNCAAAAAAAAA!!...', '', '2012-05-27 01:30:00', 1, NULL, 1, 1, NULL, NULL, NULL, '2012-05-26 23:07:39', '2012-05-27 01:30:04'),
(40, 'SIIII!! YA HABILITAMOS EL INGRESO del AREADANCE!!.. \r\n\r\nYA estamos recibiendo a todos LOS CUMPLEAÑEROS de MAYO del AREADANCE!..\r\n\r\nY en el AREAVIP estamos a MINUTOS de q comienze el RECITAL de LOS COVERNICOLAS agitando a todos LOS CUMPLEAÑERS del AREAVIP..\r\n\r\nVOS Q ESPERAS?? DALE Q LA FIESTA DE ESTA NOCHE LARGA MAS TEMPRANOOOO!!!', 'http://www.youtube.com/watch?v=LRYNWfJzfVE', '2012-05-27 02:00:00', 1, NULL, 1, 1, NULL, NULL, NULL, '2012-05-26 23:11:46', '2012-05-27 02:00:05'),
(42, 'NI QUIERO IMAGINARME cuando dentro de un rato LARGUEMOS con todo el NUEVO TEMA de AREAAAAA!!..\r\n\r\nESTA ES LA CANCION de AREA q nos IDENTIFICA a VOS y a NOSOTROS de FIESTA!..\r\n\r\nNUNCA te OLVIDES,. Q estes donde estes y ESCUCHES este TEMA, es la NUEVA CANCION de AREAAAAA!!.', 'http://www.youtube.com/watch?v=LRYNWfJzfVE', '2012-05-27 02:30:00', 1, NULL, 1, 1, NULL, NULL, NULL, '2012-05-26 23:15:50', '2012-05-27 02:30:03'),
(43, 'WAUUUUUUUUUUUU!!! ESTO SI Q ES FIESTAAAAAA AMIGOOOSSSS!!..\r\n\r\nTODA LA SEMANA ESPERE POR ESTOOO!..\r\n\r\nASI LOS QUIERO DE FIESTAAAAA!!.. ESTO ES AREAAAAAAAAAA!!!..', '', '2012-05-27 03:00:00', 1, NULL, 1, 1, NULL, NULL, NULL, '2012-05-26 23:17:25', '2012-05-27 03:00:05'),
(44, 'jajaa TE DIJE Q LA FIESTA EMPEZABA MAS TEMPRANO Q NUNA EHHH..\r\n\r\nVISTE Q NO TE MENTIMOOOSSSS!! \r\n\r\nQUE BUENOOOOO ESTAAA ESTO DIOOOSSSSSSSS!!!', '', '2012-05-27 03:30:00', 1, NULL, 1, 1, NULL, NULL, NULL, '2012-05-26 23:18:52', '2012-05-27 03:30:02'),
(45, 'LLENO TOTAAAALLLLLL!!!!!   GRACIAS AREADICTOOS POR METER TANTA FIESTAAA!!!\r\n\r\nESTO ES DE USTEDESSS AMIGOOSSS!..\r\n\r\nUSTEDES SON LA VIDA DE ESTE AREAAAAAAAAAAA!!...', '', '2012-05-27 04:00:00', 1, NULL, 1, 1, NULL, NULL, NULL, '2012-05-26 23:19:59', '2012-05-27 04:00:04'),
(46, 'ES INCREÍBLE LO Q ESTAMOS VIVIENDOOO!. \r\n\r\nNO ENTRA MAS UNA AGUJAAA!! \r\n\r\nCOMO NO VINESTE MAS TEMPRANOOO??? DALEEEE Q ESTO ESTA DE PUTA MADREEEEE!!!', '', '2012-05-27 04:30:00', 1, NULL, 1, 1, NULL, NULL, NULL, '2012-05-26 23:23:28', '2012-05-27 04:30:02'),
(47, 'BIENVENIDOS al AUTENTICO "PARAISO de las MUJERES"\r\n\r\nBIENVENIDOS AL FESTEJO de LOS CUMPLES de MAYO.!!\r\n\r\nBIENVENIDOS A CASAAAAAAAA!!...', '', '2012-05-27 05:00:00', 1, NULL, 1, 1, NULL, NULL, NULL, '2012-05-26 23:24:55', '2012-05-27 05:00:04'),
(48, 'SI SIIIIII!!! ACAAAA ESTAAAAA.. ESTE ES EL TEMAZOO de AREAAA!..\r\n\r\nESTA SONANDO EN ESTE PRESISO MOMENTO Y EXPLOTAAAAAA!!..\r\n\r\nESTE ES TU TEMAAA!! ESTE EL EL NUEVO TEMA DE AREAAAAAAAAAAA!!\r\n', 'http://www.youtube.com/watch?v=LRYNWfJzfVE', '2012-05-27 05:30:00', 1, NULL, 1, 1, NULL, NULL, NULL, '2012-05-26 23:26:52', '2012-05-27 05:30:02'),
(49, 'QUISIERA Q ESTO DURE PARA SIEMPREEEEE... \r\n\r\nCASI TANTO COMO UNA ETERNIDADDDD!!..\r\n\r\njajaa.. ojala no se terminara nunca esta nochee!!! q buenos esta estoo dioosss!.. ', '', '2012-05-27 06:00:00', 1, NULL, 1, 1, NULL, NULL, NULL, '2012-05-26 23:28:57', '2012-05-27 06:00:03'),
(50, 'REALMENTE FUE UN DIGNO FESTEJO PARA TODOS LOS "CUMPLEAÑEROS de MAYO"\r\n\r\nTAL COMO LO PLANEAMOS!.. NI MAS NI MENOS, FUE UNA VERDADERA FIESTA!!..\r\n\r\nA LOS Q YA EMPRENDEN SU VIEJE DE REGRESO LES PEDIMOS ENCARECIDAMENTE Q TENGAN MUCHISIMA PRECAUCION!!.\r\n\r\nCUÍDENSE EN LA RUTA Y ELIJAN EN LO POSIBLE AL Q NO TOMO.. O AL KE MENOS TOMO.. ', '', '2012-05-27 06:30:00', 1, NULL, 1, 1, NULL, NULL, NULL, '2012-05-26 23:32:24', '2012-05-27 06:30:04'),
(51, 'UUUUUUUYYYYYYYYY COMO ODIO Q SE HAGA DE DIA Y TENER Q TERMINAR LA FIESTAAA!!..\r\n\r\nHUBIERA DADO LO KESEA POR UNAS HORAS MAS DE CARAVANA ENTRE BUENOS AMIGOOSS!..\r\n\r\nBUEN VIAJE A TODOOOSSSS y INFINITAMENTE GRACIAS POR SER AREAAA!!..', '', '2012-05-27 07:00:00', 1, NULL, 1, 1, NULL, NULL, NULL, '2012-05-26 23:38:18', '2012-05-27 07:00:02'),
(52, 'NO EXISTEN PALABRAS PARA DESCRIBIR LO Q VIVIMOS ESTA NOCHE EN AREA!!..\r\n\r\nSOLO TODOS LOS PRESENTES SON QUIENES DEJARON TODO EN ESTA MAGNIFICA NOCCHE DE FIESTA!..\r\n\r\nAHORA SOLO FALTA ESPERAR UNA SEMANITA PARA VOLVER A REPETIR ESTA MAGICA HISTORIA DE AMIGOS FIESTEROS!..', '', '2012-05-27 07:30:00', 1, NULL, 1, 1, NULL, NULL, NULL, '2012-05-26 23:40:43', '2012-05-27 07:30:03'),
(53, 'QUE TENGAN UN DOMINGO MARAVILLOSO ENTRE FAMILIA Y AMIGOOOSSS!!..\r\n\r\nDISFRUTENLOOOO!!\r\n\r\nBUENA SEMANA PARA TODOSS LOS VAMOS A EXTRAÑAR!! \r\n', '', '2012-05-27 08:00:00', 1, NULL, 1, 1, NULL, NULL, NULL, '2012-05-26 23:42:07', '2012-05-27 08:00:02'),
(54, 'Heeeyyyy!!! que levante la mano con un ME GUSTA!\r\n\r\nEl que quiera estar de FIESTA Yaaaaaaaa!!!\r\n\r\nQueres que sea SABADO y EXPLOTAR con el NUEVO TEMA deAREA???', 'http://www.youtube.com/watch?v=LRYNWfJzfVE', '2012-05-29 19:30:00', 0, 1440, 5, 5, 1, 3, NULL, '2012-05-29 19:23:38', '2012-05-30 19:31:02'),
(56, 'Esperamos q hayas pasado una EXCELENTE SEMANA!..\r\n\r\nAhora es momento de PLANEAR el FINDEEEE!! jee\r\n\r\nMAÑANA se viene otro de los FIESTONES mas ESPERADOS por TODOS!!!', 'http://www.youtube.com/watch?v=hC_CRT8dqYc', '2012-06-01 12:30:00', 0, 10080, 3, 3, 1, 4, NULL, '2012-06-01 11:52:17', '2012-06-01 12:32:02'),
(57, 'TEMAZO PARA IR PENSANDO EN EL SABADOOOO!!\r\n\r\n▂ ▃ ▅ ▆ █ NENE MALO █ ▆ ▅ ▃ ▂ \r\n\r\nVAMOS LEVANTANDO TEMPERATURAAAAAAA!!!!', 'http://www.youtube.com/watch?v=na6-eeE99sU', '2012-06-01 13:00:00', 0, 10080, 3, 3, 1, 4, NULL, '2012-06-01 11:53:21', '2012-06-01 13:02:02'),
(58, 'UUUUUUUUUYYYYYY LLEGO LA INVACION DE LOS PONJAAASSS!!\r\n\r\nMIRA KE PEDAZOS DE BALINAZOOOS LOS PONJAS ESTOS! jaa\r\n\r\nWENO PERO SIRVE PARA ADELANTAR LA CARAVANA DE MAÑANA.. jjee', 'http://www.youtube.com/watch?v=fXsul4kfNuE&feature=fvst', '2012-06-01 13:30:00', 0, 10080, 3, 3, 1, 4, NULL, '2012-06-01 11:57:54', '2012-06-01 13:32:02'),
(59, '2 pm. DECIME si no esta weno para planear tu ESCAPADA de MAÑANA a AREAAAAAA!!! jaja\r\n\r\nAca va el temita de SHAKIRA para subir la TEMPERATURA!!\r\n\r\nNos VEMOS mañana en nuestra SEGUNDA CASA llamada AREAAA!!!', 'http://www.youtube.com/watch?v=MntbN1DdEP0&list=UUGnjeahCJW1AF34HBmQTJ-Q&index=0&feature=plcp', '2012-06-01 14:00:00', 0, 10080, 3, 3, 1, 4, NULL, '2012-06-01 12:07:34', '2012-06-01 14:02:03'),
(60, 'Hayyyy AMIGOOOSSSS!! Q GANAS de Q sea SABADO!\r\n\r\nYa HACE FALTA BAILAR!!\r\n\r\nEsta es OtRa CaNcIoN para CARAR PILAS ajaa', 'http://www.youtube.com/watch?v=RhEq01kpaFw', '2012-06-01 14:30:00', 0, 10080, 3, 3, 1, 4, NULL, '2012-06-01 13:29:03', '2012-06-01 14:32:03'),
(61, 'Q MAS SE PUEDE PEDIR?? \r\n\r\nLLEGO EL FINDEEEEEE!!! \r\n\r\nMAÑANA VOLVEMOS A VERNOOOOSSSS!!!', 'http://soundcloud.com/area-disco-san-jose', '2012-06-01 15:00:00', 0, 10080, 3, 3, 1, 4, NULL, '2012-06-01 13:32:23', '2012-06-01 15:02:08'),
(62, 'Esta noche "EL EQUIPO de AREA" sale a RECORRER toda la ZONA!!\r\n\r\nPreparate q salimos con INVITACIONES ESPECIALES + REMERAS + CONSUMICIONES\r\npara que MAÑANA vivas otro de los mejores SABADOS de tu VIDA!!..\r\n\r\nNos VEMOS esta noche BARES, PUBS y DISCOS de toda la REGION!!', 'http://www.youtube.com/watch?v=NH9_IpyDog4', '2012-06-01 16:00:00', 0, 10080, 3, 1, 1, 4, '2012-06-01 16:00:09', '2012-06-01 13:46:31', '2012-06-01 16:00:09'),
(63, 'QUE BUEN VIERNEEESSSS!!!\r\n\r\nDECI QUE ES MEDIO TEMPRANO, SINO ESTARIA WENO ARRANCAR YA CON UNA BUENA PREVIA NO?? jaa\r\n\r\nVAMOOOS AMIGOSSS SOLO FALTA 1 DIA PARA VOLVER A ESTAR EN "EL PARAISO de las MUJERES"', 'http://www.youtube.com/watch?v=nizQ4sTCeXs&feature=plcp', '2012-06-01 17:00:00', 0, 10080, 3, 1, 1, 4, '2012-06-01 17:00:06', '2012-06-01 14:04:32', '2012-06-01 17:00:06'),
(64, 'jjee! ya son LAS 6 pm.  AHORA SO NOS PODEMOS PERMITIR UN FERNESITO NO??\r\n\r\nARRIBAAAAA BARRAVALEROOOOSSSSSSS!!!\r\n\r\nVAMOS SUBIENDO DE A POQUITO Q ESTAMOS EN EL FINDEEE!!', 'http://www.youtube.com/watch?v=VTut1ilNATg&feature=fvst', '2012-06-01 18:00:00', 0, 10080, 3, 0, 1, 4, NULL, '2012-06-01 14:17:38', '2012-06-01 14:17:38'),
(65, 'bueno bueno buenoo..\r\n\r\nUn aplauso para "LA SEMANA" que se FUEEEE!!!\r\n\r\nChAu SEMANA!! Bienvenido FINDEEE!! tkm!!', 'http://www.youtube.com/watch?v=3Tt0FmrnJPA', '2012-06-01 18:30:00', 0, 10080, 3, 0, 1, 4, NULL, '2012-06-01 14:23:04', '2012-06-01 14:23:04'),
(66, 'QUE GANAS DE SALIRRRRR!! porfin llegaste FINDEEE!!..\r\n\r\nESTA NOCHE GIRA DE PUBLICAS POR TODA LA ZONA!!\r\n\r\nMAÑANA JODA AREADICTA DESDE TEMPRANO EN CASA! "AREA"', 'http://www.youtube.com/watch?v=bH7TaGSQDGc', '2012-06-01 19:00:00', 0, 10080, 3, 0, 1, 4, NULL, '2012-06-01 14:28:09', '2012-06-01 19:06:56'),
(67, 'HoLa HoLaaaa HoLaAAAA!!! FELIZ SABADO para TODOOSSS!!!..\r\n\r\nBIENVENIDOS AL SABADOO!!!\r\n\r\nQUE PEDAZO DE TARDE Q NOS TOCO VIVIR NO?? A PREPARARSE Q ESTA NOCHE ESTAMOS DE FIESTAAAAAA!!!', 'http://www.youtube.com/watch?v=RhEq01kpaFw', '2012-06-02 14:00:00', 0, 10080, 2, 1, 1, 4, '2012-06-02 14:00:04', '2012-06-02 13:44:43', '2012-06-02 14:00:04'),
(68, 'ALGUIEN Q ME DIGA Q PINTA A ESTA HORA??..\r\n\r\nMATEADA?? PICADITA CON CERVEZA?? PICADITO DE FUTBOL¿¿\r\n\r\nDECIME,.. DECIMEE.. EN QUE ANDAS?? MIENTRAS ESCUCHATE ESTO Y ADELANTATE A LA FIESTA DE ESTA NOCHE!!!', 'http://www.youtube.com/watch?v=nizQ4sTCeXs&feature=plcp', '2012-06-02 14:30:00', 0, 10080, 2, 1, 1, 4, '2012-06-02 14:30:05', '2012-06-02 13:48:39', '2012-06-02 14:30:05'),
(70, 'NO SE USTEDES,.. PERO YO YA QUIERO ESTAR DE CARAVANA EN AREA!!!\r\n\r\nVAMOS HACIENDO UN PRE CALENTAMIENTO CON ESTE TEMITA,.. LES PARECE??\r\n\r\nDISFRUTENLO Y VAYAN PREPARANDOSE PARA ESTA NOCHEEEEE!!!!', 'http://www.youtube.com/watch?v=NH9_IpyDog4', '2012-06-02 15:00:00', 0, 10080, 2, 0, 1, 4, NULL, '2012-06-02 13:52:40', '2012-06-02 13:52:40'),
(71, 'WEPPPAAAA!!! QUE RESACA NO''?? jjee\r\n\r\nSEGURO ANOCHE SALISTE Y TE VOLASTE LA PELUCA...\r\n\r\nESTA NOCHE SE REPITEEEEEE jajaaa!!! AREAAAAAAAAA!!!', 'http://www.youtube.com/watch?v=MntbN1DdEP0&list=UUGnjeahCJW1AF34HBmQTJ-Q&index=0&feature=plcp', '2012-06-02 16:00:00', 0, 10080, 3, 0, 1, 4, NULL, '2012-06-02 14:42:26', '2012-06-02 14:42:26'),
(72, 'UUYYYYY OTRA VEZ LOS PONJAAAASSS!!!\r\n\r\nLLEGO EL CLUB DE LOS BALINAZOS!!! jaa!! PONJAS BALIES!!..\r\n\r\nVEAMOS EL LADO POSITIVO DE LOS PONJAS ESTOS Y CARGUEMOS LAS PILAS PARA BAILAR ESTA NOCHEEEE!!!', 'http://www.youtube.com/watch?v=i6mpjMTbKDw', '2012-06-02 17:00:00', 0, 10080, 2, 0, 1, 4, NULL, '2012-06-02 14:46:37', '2012-06-02 14:46:37'),
(73, 'HEEE MUJERES!!! EMPIECEN A BAILARRRRR!!!..\r\n\r\nVAMOS QUE ESTA NOCHE VOLVEMOS A ESTAR DE CARAVANA!!!..\r\n\r\nCALENTEMOS MOTORES ROCHAS Y CHETAASSS AREADICTAS!!! ', 'http://www.youtube.com/watch?v=na6-eeE99sU', '2012-06-02 18:00:00', 0, 10080, 2, 0, 1, 4, NULL, '2012-06-02 14:49:06', '2012-06-02 14:49:06'),
(74, 'wauuuu!! LLEGO el SABADOOOOOO!!!\r\n\r\nESTA NOCHE VOLVEMOS al PARAISO DE LAS MUJERES!!\r\n\r\nESTA NOCHE VAMOS A BAILAR "EL NUEVO TEMAZO de AREA"', 'http://www.youtube.com/watch?v=LRYNWfJzfVE', '2012-06-02 19:00:00', 0, 10080, 2, 0, 1, 4, NULL, '2012-06-02 14:51:58', '2012-06-02 14:51:58'),
(75, 'SON LAS 21hs Y CREO Q ES LA HORA JUSTA PARA BAILAR EL NUEVO EXITO DE "Wisin & Yandel - Follow The Leader JUNTO A LA DIVA Jennifer Lopez!!!..\r\n\r\nYA COMENZO LA CUANTA REGRESIVA A LA FIESTA DE ESTA NOCHE!!!.. \r\n\r\nVAMOS AREAAAAA!!! A CARGAR PILAAAASSSS!!!!', 'http://www.youtube.com/watch?v=Xmap94TcDNs&list=UUPypl5EbwJfZzDOT_Ln-_QA&index=1&feature=plcp', '2012-06-02 21:00:00', 0, 10080, 2, 0, 1, 4, NULL, '2012-06-02 14:58:57', '2012-06-02 14:58:57'),
(76, 'YA PLANEASTE CON QUIENES VENIS ESTA NOCHE???\r\n\r\nDALE Q FALTAN POCAS HORAS PARA VOLVER A ESTAR DE FIESTA EN AREA!\r\n\r\nURGENTE MENSAJEA Y ORGNIZATE PARA ESTA NOCHE!!!', 'http://www.youtube.com/watch?v=B1IN4S6O6sA', '2012-06-02 20:00:00', 0, 10080, 2, 0, 1, 4, NULL, '2012-06-02 15:05:01', '2012-06-02 15:05:01'),
(77, 'VAMOOOS AREADICTOS QUE YA FALTA MUY POQUITO PAEA VOLVER A ESTAR DE FIESTA EN CASA.!\r\n\r\nESTA NOCHE ARRANCAMOS TEMPRANITO CON EL RECITAL EN VIVO DE "ELIAZIM RIVERA" de OP.TRIUNFO PRIMERO EN "AREAVIP" Y MAS TARDE EN "AREADANCE"\r\n\r\nY PREPARATE A BAILAR EL NUEVO TEMA DE AREAAAA CUANDO LARGUEMOS LA NOCHE CON TODOO!!!\r\n\r\n', 'http://www.youtube.com/watch?v=LRYNWfJzfVE', '2012-06-02 22:25:00', 1, NULL, 1, 0, NULL, NULL, NULL, '2012-06-02 22:19:38', '2012-06-02 22:20:03'),
(79, 'HOOLLAAAAAA HOLAAAA HOLAAAA!!! \r\n\r\nBUEN DIAAAAAA PARA TODOOOSS!!!..\r\n\r\nBIENVENIDA LA FRESCA! jjaaaa...', '', '2012-06-06 10:25:00', 1, NULL, 1, 1, NULL, NULL, '2012-06-06 10:25:04', '2012-06-06 10:20:31', '2012-06-06 10:25:04');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
