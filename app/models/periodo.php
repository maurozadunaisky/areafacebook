<?php

class Periodo extends AppModel {

	var $hasMany = array('Post');

	var $brwConfig = array(
		'names' => array(
			'plural' => 'Periodos',
			'singular' => 'Periodo',
		),
		'actions' => array(
			'add' => false,
			'delete' => false,
			'edit' => false,
		),
	);

}