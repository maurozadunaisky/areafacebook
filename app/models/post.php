<?php

class Post extends AppModel {

	var $order = array('Post.fecha_hora_publicacion' => 'desc');

	var $belongsTo = array('Periodo');

	var $validate = array(
		'link' => array(
			'rule' => array('url', true),
			'allowEmpty' => true,
			'message' => 'URL no válida',
		),
		'repetir_cada_show' => array(
			array(
				'rule' => 'notEmpty',
				'message' => 'Campo requerido',
			),
		),
		'cant_repeticiones' => array(
			array(
				'rule' => 'mayorA2',
				'message' => 'La cantidad tiene que ser mayor o igual a 2',
			),
			array(
				'rule' => 'notEmpty',
				'message' => 'Campo requerido',
			),
		),
		'periodo_id' => array(
			array(
				'rule' => 'notEmpty',
				'message' => 'Campo requerido',
			),
		),
	);

	var $brwConfig = array(
		'names' => array(
			'plural' => 'Posteos',
			'singular' => 'Posteo',
		),
		'fields' => array(
			'no_editor' => array('mensaje'),
			'no_add' => array('cant_publicadas', 'fecha_hora_ultima_publicacion'),
			'no_edit' => array('cant_publicadas', 'fecha_hora_ultima_publicacion'),
			'filter' => array('fecha_hora_publicacion'),
			'hide' => array('repetir_cada'),
			'names' => array(
				'repetir_cada_show' => 'Repetir cada',
				'una_vez' => '¿Publicar el post una sola vez?'
			),
			'conditional' => array(
				'una_vez' => array(
					'hide' => array('cant_repeticiones', 'repetir_cada_show', 'periodo_id',),
					'show_conditions' => array(
						0 => array('cant_repeticiones', 'repetir_cada_show', 'periodo_id'),
						1 => array(),
					),
				),
			),
		),
		'paginate' => array(
			'fields' => array('id', 'mensaje', 'fecha_hora_publicacion'),
			'images' => array('Foto'),
		),
		'images' => array(
			'Foto' => array(
				'sizes' => array('100_100', '1024_1024'),
				'description' => false,
				'index' => true,
			),
		),
	);

	function beforeValidate(){
		if(isset($this->data['Post']['una_vez']) and $this->data['Post']['una_vez'] == 1){
			unset($this->validate['repetir_cada_show']);
			unset($this->validate['cant_repeticiones']);
			unset($this->validate['periodo_id']);
		} elseif (isset($this->data['Post']['una_vez']) and $this->data['Post']['una_vez'] == 0) {

			if (isset($this->data['Post']['periodo_id']) and $this->data['Post']['periodo_id'] == 1) {
				$this->validate['repetir_cada_show'][] = array(
					'rule' => 'minimo30min',
					'message' => 'La cantidad tiene que ser mayor o igual a 30 min',
				);
			}

		}
		return true;
	}


	function beforeSave() {
		$this->whitelist = array(
            'id',
            'mensaje',
            'link',
            'fecha_hora_publicacion',
            'una_vez',
			'repetir_cada',
            'cant_repeticiones',
            'cant_publicadas',
            'repetir_cada_show',
            'periodo_id',
            'name',
            'model',
            'category_code',
            'description',
            'record_id',
            'fecha_hora_ultima_publicacion',
            'created',
            'modified',

        );
       if (empty($this->data['Post']['una_vez'])) {
			if (!empty($this->data['Post']['repetir_cada_show']) and !empty($this->data['Post']['periodo_id'])) {
				$this->data['Post']['repetir_cada'] = $this->_cantidadDeMinutos($this->data['Post']['repetir_cada_show'], $this->data['Post']['periodo_id']);
			}
		} else {
			$this->data['Post']['cant_repeticiones'] = 1;
			$this->data['Post']['repetir_cada'] = null;
			$this->data['Post']['repetir_cada_show'] = null;
			$this->data['Post']['periodo_id'] = null;
		}
		return parent::beforeSave();
	}


	function postNext() {
		$post = $this->postAvailable(date('Y-m-d H:i:s'));
		if ($post) {
			$result = $this->postToFacebook($post);
			$cant_publicadas =  ($post['Post']['cant_publicadas'] +  1);
			$this->save(array(
				'id' => $post['Post']['id'],
				'cant_publicadas' => $cant_publicadas,
				'fecha_hora_ultima_publicacion' => date('Y-m-d H:i:s'),
			));
		} else {
			$result = false;
		}
		return array('post' => $post, 'result' => $result);
	}


	function postToFacebook($post) {
		App::import('Lib', 'facebook/facebook');
		$facebook = new Facebook(array('appId' => $this->fb['appId'], 'secret' => $this->fb['secret']));
		foreach ($this->fb['users'] as $user) {
			$params = array(
			    'access_token' => $user['token'],
				'message' => $post['Post']['mensaje'],
			);
			if ($post['BrwImage']['Foto']) {
				$facebook->setFileUploadSupport(true);
				$params['image'] = '@' . $post['BrwImage']['Foto']['realpath'];
				$facebook->api('/' . $user['userId'] . '/photos', 'post', $params);
			} else {
				if ($post['Post']['link']) {
					$params['link'] = $post['Post']['link'];
				}
				$facebook->api('/' . $user['userId'] . '/feed', 'post', $params);
			}
		}
		return true;
	}


	function postAvailable($fechaHoraActual){
		$posts = $this->find('all', array(
			'conditions' => array(
				'Post.fecha_hora_publicacion <=' => $fechaHoraActual,
				array("Post.cant_publicadas < Post.cant_repeticiones"),
			),
			'contain' => array('BrwImage'),
		));
		$postAvailable = false;
		foreach($posts as $key => $post){
			// si se publico no se vuelva a publicar hasta que no pase un timepo de $post['Post']['repetir_cada']
			if (empty($post['Post']['fecha_hora_ultima_publicacion']) or (!empty($post['Post']['fecha_hora_ultima_publicacion']) and
				(date('Y-m-d H:i:s', strtotime($post['Post']['fecha_hora_ultima_publicacion']. ' +'.($post['Post']['repetir_cada']).' minutes')) <= $fechaHoraActual))) {

				for($i = 0; $i <= ($post['Post']['cant_repeticiones'] - 1); $i++) {
					$horariosPosibles[$i]['desde'] = date('Y-m-d H:i:s', strtotime($post['Post']['fecha_hora_publicacion']. ' +'.($i * $post['Post']['repetir_cada']).' minutes'));
					$horariosPosibles[$i]['hasta'] = date('Y-m-d H:i:s', strtotime($post['Post']['fecha_hora_publicacion']. ' +'.(($i * $post['Post']['repetir_cada']) + 4) .' minutes'));
				}

				$ultimaRepeticion = end($horariosPosibles);
				if ($ultimaRepeticion['hasta'] >= $fechaHoraActual) {
					foreach($horariosPosibles as $horarioPosible){
						if($horarioPosible['desde'] <= $fechaHoraActual and $fechaHoraActual <= $horarioPosible['hasta']){
							$postAvailable = $posts[$key];
						}
					}
				}
			} else {
				unset($posts[$key]);
			}
		}

		return $postAvailable;
	}


	function minimo30min($data) {
		if (!empty($data['repetir_cada_show'])) {
			if ($data['repetir_cada_show']>= 30) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}


	function mayorA2($data) {
		if (!empty($data['cant_repeticiones'])) {
			if ($data['cant_repeticiones']>= 2) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}


	function _cantidadDeMinutos($repetir_cada_show, $periodo){
		$cantiMinutos = 0;
		switch($periodo){
			//Minutos
			case 1:
				$cantiMinutos = $repetir_cada_show * 1;
			break;
			//Horas
			case 2:
				$cantiMinutos = $repetir_cada_show * 60;
			break;
			//Días
			case 3:
				$cantiMinutos = $repetir_cada_show * 60 * 24;
			break;
			//Semanas
			case 4:
				$cantiMinutos = $repetir_cada_show * 60 * 24 * 7;
			break;
		} // switch

		return $cantiMinutos;
	}


}