<?php

class PostsController extends AppController {

	var $layout = 'empty';

	function run() {
		$this->set('post', $this->Post->postNext());
	}


	function add() {
		App::import('Lib', 'facebook/facebook');
		$facebook = new Facebook(array('appId' => $this->Post->fb['appId'], 'secret' => $this->Post->fb['secret']));
		$this->set(array(
			//'loginUrl' => $facebook->getLoginUrl(array('canvas' => 1, 'fbconnect' => 0, 'scope' => 'offline_access,publish_stream')),
			'appId' => $this->Post->fb['appId'],
			'user' => $facebook->getUser(),
			'token' => $facebook->getAccessToken()
		));
	}

}