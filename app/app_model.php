<?php
App::import('Lib', 'LazyModel.LazyModel');

class AppModel extends LazyModel {

	var $brwConfig = array();
	var $recursive = -1;
	var $actsAs = array(
		'Containable',
		'Brownie.Panel',
	);

	var $fb = array(
		'appId' => '258370534202001',
		'secret' => '5ac77c996859efa5a08362ac8e0d839e',
		'users' => array(
			1 => array(
				'userId' => '1684553995',
				//'token' => 'AAADqZCJPpWpEBANAJwsZCEafoiwLWMj4llyvATQL9AUincyZCjVQKrZCzCBC9SrxR4zRTz266z732x1XMmwWZBYwnheNXOwIZD',
				'token' => 'AAADqZCJPpWpEBAA8mZBf9HbcZCx50H07Oj5OtvUkJQGs4TRGqDAok5wFZA1COjNM4UrLC177nAlqafXO9PZBn92Qfs8ZAhfiYZD',
			),
			2 => array(
				'userId' => '100002553234252',
				'token' => 'AAADqZCJPpWpEBAIWxa3O4h6Uvl1bZATjnzCW5zAELsaubAVevv6gIQRu7SZCu5FlRErUmGwNXnv4jFkutsRUGZA96hzeYgEQBFpDT42U1XqZA4DCTPefG',
			),
		),
	);

	/*
	var $fb = array(
		'appId' => '258370534202001',
		'secret' => '5ac77c996859efa5a08362ac8e0d839e',
		'userId' => '100000562802422',
		//'token' => 'AAADqZCJPpWpEBANeUxZAM21F8Adjd5RJtiVBCCUshXjlpQatZAaz2ToCTxxlFUMWyqTkLKce28PSBbn8HQhiG44JOk9bCSWbU0ZB6GYAZAgZDZD',
		'token' => 'AAADqZCJPpWpEBAA8mZBf9HbcZCx50H07Oj5OtvUkJQGs4TRGqDAok5wFZA1COjNM4UrLC177nAlqafXO9PZBn92Qfs8ZAhfiYZD',
	);*/


}