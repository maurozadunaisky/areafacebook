<?php
/* Post Test cases generated on: 2012-05-11 12:05:54 : 1336750134*/
App::import('Model', 'Post');

class PostTestCase extends CakeTestCase {
	var $fixtures = array(
		'app.post',
		'app.brw_image',
		'app.brw_file',
		'app.brw_user',
		'app.periodo',
	);

	function startTest() {
		$this->Post =& ClassRegistry::init('Post');
	}

	function endTest() {
		unset($this->Post);
		ClassRegistry::flush();
	}

	function testPostNext() {

	}

	function testPostToFacebook() {

	}

	function testPostAvailable() {
		//fecha que no hay post
		$postAvailable = $this->Post->postAvailable('2010-10-27 20:00:00');
		$this->assertFalse($postAvailable);

		//el mensaje tiene fecha_hora_ultima_publicacion en null asi que entre las horas desde y hasta tiene que mandar siempre
		$postAvailable = $this->Post->postAvailable('2010-10-28 19:41:00');
		$this->assertFalse($postAvailable);

		$postAvailable = $this->Post->postAvailable('2010-10-28 19:46:00');
		$this->assertTrue($postAvailable['Post']['id'] == 1);

		$postAvailable = $this->Post->postAvailable('2010-10-28 19:49:00');
		$this->assertFalse($postAvailable);

		$postAvailable = $this->Post->postAvailable('2010-10-28 20:06:00');
		$this->assertTrue($postAvailable['Post']['id'] == 1);

		$postAvailable = $this->Post->postAvailable('2010-10-28 20:10:00');
		$this->assertFalse($postAvailable);

		$postAvailable = $this->Post->postAvailable('2010-10-28 20:26:00');
		$this->assertTrue($postAvailable['Post']['id'] == 1);

		$postAvailable = $this->Post->postAvailable('2010-10-28 20:29:00');
		$this->assertFalse($postAvailable);

		//el mensaje tiene fecha_hora_ultima_publicacion definida

		$postAvailable = $this->Post->postAvailable('2010-10-28 10:18:00');
		$this->assertFalse($postAvailable);

		$postAvailable = $this->Post->postAvailable('2010-10-28 10:22:00');
		$this->assertTrue($postAvailable['Post']['id'] == 2);

		$postAvailable = $this->Post->postAvailable('2010-10-28 10:28:00');
		$this->assertFalse($postAvailable);

		$postAvailable = $this->Post->postAvailable('2010-10-28 10:42:00');
		$this->assertTrue($postAvailable['Post']['id'] == 2);

		//horio de un post que ya tiene la cant_posteada igual que la cant_repeticiones

		$postAvailable = $this->Post->postAvailable('2010-10-29 17:02:00');
		$this->assertFalse($postAvailable);

		//una vez mandado tien que esperar a que pase el tiempo de 'repetir cada' hasta volver a mandar

		$postAvailable = $this->Post->postAvailable('2010-10-30 14:02:00');
		$this->assertFalse($postAvailable);

		$postAvailable = $this->Post->postAvailable('2010-10-30 14:21:00');
		$this->assertTrue($postAvailable['Post']['id'] == 4);

	}

}
?>