<?php
/* Post Fixture generated on: 2012-05-11 12:05:44 : 1336750004 */
class PostFixture extends CakeTestFixture {
	var $name = 'Post';
	var $import = array('model' => 'Post');


	var $records = array(
		array(
			'id' => 1,
			'mensaje' => 'asdfasdf\r\n\r\nasdfasdf',
			'link' => '',
			'fecha_hora_publicacion' => '2010-10-28 19:44:00',
			'repetir_cada' => '20',
			'cant_repeticiones' => 3,
			'cant_publicadas' => 0,
			'fecha_hora_ultima_publicacion' => null,
			'created' => '2011-10-04 19:48:21',
			'modified' => '2011-10-04 20:28:17'
		),
		array(
			'id' => 2,
			'mensaje' => '2do mensaje',
			'link' => '',
			'fecha_hora_publicacion' => '2010-10-28 10:20:00',
			'repetir_cada' => '20',
			'cant_repeticiones' => 5,
			'cant_publicadas' => 4,
			'fecha_hora_ultima_publicacion' => null,
			'created' => '2011-10-04 19:48:21',
			'modified' => '2011-10-04 20:28:17'
		),
		array(
			'id' => 3,
			'mensaje' => '3ro mensaje',
			'link' => '',
			'fecha_hora_publicacion' => '2010-10-29 17:00:00',
			'repetir_cada' => '20',
			'cant_repeticiones' => 2,
			'cant_publicadas' => 2,
			'fecha_hora_ultima_publicacion' => null,
			'created' => '2011-10-04 19:48:21',
			'modified' => '2011-10-04 20:28:17'
		),
		array(
			'id' => 4,
			'mensaje' => '4ro mensaje',
			'link' => '',
			'fecha_hora_publicacion' => '2010-10-30 14:00:00',
			'repetir_cada' => '20',
			'cant_repeticiones' => 2,
			'cant_publicadas' => 1,
			'fecha_hora_ultima_publicacion' => '2010-10-30 14:01:00',
			'created' => '2011-10-04 19:48:21',
			'modified' => '2011-10-04 20:28:17'
		),
	);
}
?>