<?php
/* Periodo Fixture generated on: 2012-05-23 11:05:49 : 1337782549 */
class PeriodoFixture extends CakeTestFixture {
	var $name = 'Periodo';
	var $import = array('model' => 'Periodo');


	var $records = array(
		array(
			'id' => 1,
			'name' => 'Minuto/s',
			'created' => '2012-05-22 12:57:12',
			'modified' => '2012-05-22 12:57:12'
		),
		array(
			'id' => 2,
			'name' => 'Hora/s',
			'created' => '2012-05-22 12:57:25',
			'modified' => '2012-05-22 12:57:25'
		),
		array(
			'id' => 3,
			'name' => 'Día/s',
			'created' => '2012-05-22 12:57:52',
			'modified' => '2012-05-22 12:57:52'
		),
		array(
			'id' => 4,
			'name' => 'Semana/s',
			'created' => '2012-05-22 12:58:09',
			'modified' => '2012-05-22 12:58:09'
		),
	);
}
?>